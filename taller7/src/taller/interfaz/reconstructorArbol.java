package taller.interfaz;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.io.IOException;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import jdk.nashorn.internal.ir.ObjectNode;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import java.io.FileWriter;


public class reconstructorArbol implements IReconstructorArbol{

	
	private ArrayList preorden;
	private ArrayList inorden;
	private nodo arbol;
	
	
	
	public reconstructorArbol()
	{
		
		arbol = new nodo();
		
		preorden = new ArrayList();
		inorden = new ArrayList();
		
		
	}
	
	
	public void cargarArchivo(String nombre) throws IOException
	
	{
		BufferedReader br = new BufferedReader(new FileReader("./data/"+nombre));
		String[] a = null;
		String[] b = null;
 		for(int i = 0; i <2; i++)
		{
			String linea = br.readLine();
			
			if(i == 0)
			{
				String[] part1 = linea.split("preorden=");
				a = part1[1].split(",");
			}
			else
			{
				String[] part1 = linea.split("inorden=");
				b = part1[1].split(",");
			}
			
		}
 		
 		for (int i = 0 ; i < a.length ; i++)
 		{
 			System.out.println(a[i]);
 			System.out.println(b[i]);
 			preorden.add(a[i]);
 			inorden.add(b[i]);
 		}
 		
		
	}
	
	public void crearArchivo(String info) throws FileNotFoundException, UnsupportedEncodingException
	
	{
		JSONObject obj = new JSONObject();
		nodo ar = new nodo();
		ar = arbol;
		obj = reco(ar);
		
		try
		{
			FileWriter file= new FileWriter(info);
			file.write(obj.toJSONString());
			file.flush();
			file.close();
		}
		catch(IOException e)
		{
			
		}
		
		
		
	}
	
	public JSONObject reco(nodo a)
	{
		
		JSONArray aa = new JSONArray();
		JSONObject ret = new JSONObject();
		
		
		if(a.retDer()!= null)
		{
			aa.add(reco(a.retDer()));
			ret.put("Children",aa);
		}
		if(a.retIzq()!= null)
		{
			aa.add(reco(a.retIzq()));
			ret.put("Children",aa);
		}
		
		
		ret.put("", a.retRaiz());
		
		return ret;
	}
	
	
	public nodo reconstruir(ArrayList in)
	{
	
		if(in.size()<2)
		{
			nodo ret = new nodo();
			ret.setraiz((String)in.get(0));
			return ret;
		}
		else
		{
			nodo nose = new nodo();
			if(in == inorden)
			{
				nose.setraiz((String)preorden.get(0));
			}
			
			int posicion = preorden.size();
			int posicion2 = 0;
			for (int i = 0 ; i < in.size() ; i++)
			{
				
				for(int j = 0; j < preorden.size(); j++)
				{
					String comparar = (String) in.get(i);
					String comparar2 = (String) preorden.get(j);
					if(posicion > j && comparar.equals(comparar2))
					{
						posicion2 = i;
						posicion = j;
					}
				}
			}
			
			ArrayList izq = new ArrayList();
			ArrayList der = new ArrayList();
			nose.setraiz((String)in.get(posicion2));
			for(int i = 0 ; i < posicion2; i++)
			{
				izq.add(in.get(i));
				
			}
			for(int i = posicion2+1 ; i < in.size(); i++)
			{
				der.add(in.get(i));
				
			}
			
			if(izq.size()>=1)
			{
			nose.setizq(reconstruir(izq));
			}
			if(der.size()>=1)
			{
			nose.setder(reconstruir(der));
			}
			return nose;
		}
	}

	public void reconstruir()
	{
		arbol = reconstruir(inorden);
		System.out.println("hola");
		
	}
	
}

	